resource "aws_s3_bucket" "blog-remote-state" {
  bucket        = "blog-remote-state"
  force_destroy = true

  tags = {
    Name = "Remote State Storage for blog"
  }
}

resource "aws_s3_bucket_public_access_block" "blog-remote-state-public-access-block" {
  bucket = aws_s3_bucket.blog-remote-state.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
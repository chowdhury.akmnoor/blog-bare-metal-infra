# AWS Subnet Resources

resource "aws_subnet" "blog-public-subnet" {
  vpc_id = aws_vpc.blog-primary.id
  cidr_block = var.public-subnet-cidr-block
  availability_zone = var.avail-zone

  tags = {
    Name = "blog-${var.env}-public-subnet"
  }
}

resource "aws_subnet" "blog-private-subnet" {
  vpc_id = aws_vpc.blog-primary.id
  cidr_block = var.private-subnet-cidr-block
  availability_zone = var.avail-zone

  tags = {
    Name = "blog-${var.env}-private-subnet"
  }
}

# AWS Gateways

resource "aws_internet_gateway" "blog-igw" {
  vpc_id = aws_vpc.blog-primary.id

  tags = {
    Name = "blog-${var.env}-igw"
  }
}

resource "aws_eip" "blog-nat-eip" {
  vpc = true

  depends_on = [
    aws_internet_gateway.blog-igw
  ]
}

resource "aws_nat_gateway" "blog-natg" {
  subnet_id = aws_subnet.blog-public-subnet.id
  allocation_id = aws_eip.blog-nat-eip.id

  depends_on = [
    aws_internet_gateway.blog-igw,
    aws_eip.blog-nat-eip
  ]

  tags = {
    Name = "blog-${var.env}-natG"
  }
}

# AWS Route Table Resources

resource "aws_route_table" "blog-public-rtb" {
  vpc_id = aws_vpc.blog-primary.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.blog-igw.id
  }

  tags = {
    Name = "blog-${var.env}-public-route-table"
  }
}

resource "aws_route_table" "blog-private-rtb" {
  vpc_id = aws_vpc.blog-primary.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.blog-natg.id
  }

  tags = {
    Name = "blog-${var.env}-private-route-table"
  }
}

# AWS Route Table Association

resource "aws_route_table_association" "blog-public-rtb-assoc" {
  subnet_id = aws_subnet.blog-public-subnet.id
  route_table_id = aws_route_table.blog-public-rtb.id
}

resource "aws_route_table_association" "blog-private-rtb-assoc" {
  subnet_id = aws_subnet.blog-private-subnet.id
  route_table_id = aws_route_table.blog-private-rtb.id
}
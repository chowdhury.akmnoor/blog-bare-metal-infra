# AWS VPC Resource

resource "aws_vpc" "blog-primary" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "blog-${var.env}-primary-vpc"
  }
}
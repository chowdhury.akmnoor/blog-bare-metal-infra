output "public-subnet" {
  value = aws_subnet.blog-public-subnet
}

output "private-subnet" {
  value = aws_subnet.blog-private-subnet
}

output "vpc" {
  value = aws_vpc.blog-primary
}
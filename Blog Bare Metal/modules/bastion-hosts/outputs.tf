
output "bastion-host" {
  value = aws_instance.blog-bastion
}

output "bastion-key" {
  value = aws_key_pair.blog-host-key
}

output "blog-server-sg" {
  value = aws_security_group.blog-server-sg
}
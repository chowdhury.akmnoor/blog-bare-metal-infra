
# AWS Security Groups

resource "aws_security_group" "blog-bastion-sg" {
  name = "blog Bastion Security Group"
  description = "Allows SSH, HTTP and HTTPS for Bastion Host"
  vpc_id = var.vpc_id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]

    description = "SSH Inbound Rule"
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]

    description = "HTTP Port Inbound Rule"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]

    description = "Outbound Rule"
  }

  tags = {
    Name = "blog Bastion SG"
  }

}


resource "aws_security_group" "blog-server-sg" {
  name = "blog Server Security Group"
  description = "Allows SSH, HTTP and HTTPS for Server Host"
  vpc_id = var.vpc_id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]

    description = "SSH Inbound Rule"
  }

  ingress {
    from_port = 3000
    to_port = 3000
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]

    description = "HTTP Port Inbound Rule"
  }

  ingress {
    from_port = 4000
    to_port = 4000
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]

    description = "HTTP Port Inbound Rule"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]

    description = "Outbound Rule"
  }

  tags = {
    Name = "blog Server SG"
  }

}
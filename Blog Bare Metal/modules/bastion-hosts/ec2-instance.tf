
# Key Pair

resource "aws_key_pair" "blog-host-key" {
  key_name = "blog-host-key"
  public_key = file(var.public-key-location)
}

# AWS EC2 Instance

resource "aws_instance" "blog-bastion" {
  ami = data.aws_ami.latest-ubuntu-ami.id
  instance_type = var.instance-type
  subnet_id = var.subnet-id
  associate_public_ip_address = var.public-ip-association
  key_name = aws_key_pair.blog-host-key.key_name
  vpc_security_group_ids = [ aws_security_group.blog-bastion-sg.id ]

  tags = {
    Name = "${var.env}-blog-bastion"
    Environment = "${upper(var.env)}"
  }

  user_data = file("./modules/bastion-hosts/user-data/script.sh")
}

variable "env" {}
variable "instance-type" {}
variable "subnet-id" {}
variable "ami-image-name" {}
variable "public-key-location" {}
variable "vpc_id" {}
variable "public-ip-association" {}
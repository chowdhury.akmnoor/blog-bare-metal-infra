#!/bin/bash

sudo apt update -y
sudo apt install -y docker.io
sudo systemctl start docker.service
sudo usermod -aG docker ubuntu

home=/home/ubuntu
nodev='16'
nvmv='0.33.11'

su - ubuntu -c "curl https://raw.githubusercontent.com/creationix/nvm/v${nvmv}/install.sh | bash"
su - ubuntu -c "nvm install ${nodev}"
su - ubuntu -c "nvm use ${nodev}"
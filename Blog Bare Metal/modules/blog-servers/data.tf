
# EC2 Instance AMI

data "aws_ami" "latest-ubuntu-ami" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = [ var.ami-image-name ]
  }

  filter {
    name = "virtualization-type"
    values = [ "hvm" ]

  }
}


# AWS EC2 Instance

resource "aws_instance" "blog-servers" {
  ami = data.aws_ami.latest-ubuntu-ami.id
  instance_type = var.instance-type
  subnet_id = var.subnet-id
  associate_public_ip_address = var.public-ip-association
  key_name = var.blog-host-key-name
  vpc_security_group_ids = [ var.blog-server-sg ]

  tags = {
    Name = "${var.env}-blog-${var.microservice}-server"
    Environment = "${upper(var.env)}"
  }

  user_data = file("./modules/blog-servers/user-data/script.sh")
}
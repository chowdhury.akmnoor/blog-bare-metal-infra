variable "env" {}
variable "microservice" {}
variable "bastion-instance-type" {}
variable "server-instance-type" {}
variable "instance-type" {
    type = map(string)
}
#variable "public-subnet-id" {}
variable "ami-image-name" {}
variable "public-key-location" {}
#variable "vpc_id" {}

variable "vpc_cidr_block" {}
variable "public-subnet-cidr-block" {}
variable "private-subnet-cidr-block" {}
variable "avail-zone" {}
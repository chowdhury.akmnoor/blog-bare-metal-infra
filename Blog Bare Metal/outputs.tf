output "bastion-ip" {
  value = module.blog-bastion-host.bastion-host.public_ip
}

output "server" {
  value = {
    for group in module.blog-server: group.blog-servers.tags.Name => group.blog-servers.public_ip
  }
}


# blog Bastion Host

module "blog-bastion-host" {
  source = "./modules/bastion-hosts"

  env = var.env
  instance-type =var.instance-type["bastion"]
  subnet-id = module.blog-vpc.public-subnet.id
  ami-image-name = var.ami-image-name
  public-key-location = var.public-key-location
  vpc_id = module.blog-vpc.vpc.id
  public-ip-association = true
}

# blog Server Hosts

locals {
  microservices = ["frontend", "backend", "admin"]
}

module "blog-server" {
  source = "./modules/blog-servers"

  env = var.env

  for_each = toset(local.microservices)
  microservice = each.value
  instance-type =var.instance-type["app-host"]
  subnet-id = module.blog-vpc.public-subnet.id
  ami-image-name = var.ami-image-name
  blog-host-key-name = module.blog-bastion-host.bastion-key.key_name
  vpc_id = module.blog-vpc.vpc.id
  public-ip-association = true
  blog-server-sg = module.blog-bastion-host.blog-server-sg.id
}
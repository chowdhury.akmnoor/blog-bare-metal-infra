bastion-instance-type = "t2.micro"
server-instance-type = "t2.medium"
ami-image-name = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
public-key-location = "./modules/bastion-hosts/id_rsa.pub"
vpc_cidr_block = "10.0.0.0/16"
public-subnet-cidr-block = "10.0.10.0/24"
private-subnet-cidr-block = "10.0.20.0/24"
avail-zone = "us-east-2a"
instance-type = {
    "bastion" = "t2.small"
    "app-host" = "t2.medium"
}


env = "dev"
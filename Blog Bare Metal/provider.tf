terraform {
  /*
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~>4.0"
    }
  }
  */
  required_version = ">=0.12"

  backend "s3" {
    bucket = "blog-remote-state"
    key = "blog_state_files/bare-metal"
    region = "us-east-2"
  }
}

provider "aws" {
  region = "us-east-2"
}
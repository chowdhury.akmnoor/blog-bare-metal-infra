# blog VPC Module

module "blog-vpc" {
  source = "./modules/vpc"

  env = var.env
  vpc_cidr_block = var.vpc_cidr_block
  public-subnet-cidr-block = var.public-subnet-cidr-block
  private-subnet-cidr-block = var.private-subnet-cidr-block
  avail-zone = var.avail-zone
}
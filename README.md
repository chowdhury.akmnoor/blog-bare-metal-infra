Add your ssh public key in `Blog\ Bare\ Metal/modules/bastion-hosts/` location

Install aws cli
Configure AWS credentials. . . .

```
aws configure
```

Run these commands
```
cd TF\ Remote\ State\ S3/
terraform init
terraform plan
terraform apply --auto-approve

cd ../Blog\ Bare\ Metal/
terraform init
terraform plan -var-file dev.tfvars
terraform apply -var-file dev.tfvars --auto-approve
```
